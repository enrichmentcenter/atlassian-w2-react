type IClientPluginsDescriptor = {
    id: string;
    location: string;
}; 

interface IClientLocationConstructor {
    new (name: string): IClientLocationInterface;
}

interface IClientLocationInterface {
    onUpdate: (callback: (clientPlugins: Array<IClientPluginsDescriptor>) => void) => void;
}

interface Window {
    __atlassian_plugins_api: {
        ClientLocation: IClientLocationConstructor;
        // ClientWebfragment: fn;
        // WebPanel: fn;
        registry: {
            register: (webfragment: any) => void;
            renderWebfragment: (id: string, container: HTMLElement, context: any) => void;
        };
    };
}