## Atlassian W2 React Components

### Installation
```
$ npm install @atlassian/atlassian-w2-react
```

### <ClientLocation />
React component creates client location.

**Example**
```
<ClientLocation name="my-client-location">
    {plugins => (
        <div>
            My Client Location wrapper
            {plugins.map(plugin => <ClientWebfragment descriptor={plugin} context={customContext} />)}
        </div>
    )}
</ClientLocation>
```

##### name

Type: `string`
__Required__
Name of client location.

##### children
Type: `function`
Render function. By default renders all plugins for that location.



### <ClientWebfragment />
Client Webfragment react wrapper.

##### descriptor

Type: `object`
__Required__
Webfrgament descriptor. Should contain: `{id: string}`

#### context

Type: `any`
Will be passed as a context in your webfragment component.