import React from 'react';
import { registry } from './registry';

interface IClientWebfragmentProps {
    context?: any;
    descriptor: IClientPluginsDescriptor;
}

class ClientWebfragment extends React.Component<IClientWebfragmentProps> {
    container: HTMLDivElement;
    
    componentDidMount() {
        const {context, descriptor} = this.props;

        registry.renderWebfragment(descriptor.id, this.container, context);
    }

    _refCallback = (ref: HTMLDivElement) => {
        if (!ref) {
            return;
        }

        this.container = ref;
    }

    render() {
        return (
            <div ref={this._refCallback} />
        );
    }
}

export default ClientWebfragment;