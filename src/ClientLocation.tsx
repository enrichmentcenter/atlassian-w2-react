import React from 'react';
import ClientWebfragment from './ClientWebfragment';
import { ClientLocation as ClientLocationConstructor, registry } from './registry';


interface IClientLocationProps {
    name: string;
    children: (plugins: Array<IClientPluginsDescriptor>) => React.ReactElement | null;
}

interface IClientLocationState {
    clientPlugins: Array<IClientPluginsDescriptor> | null
}

class ClientLocation extends React.Component<IClientLocationProps, IClientLocationState> {
    state = {
        clientPlugins: null
    };

    location: any;

    componentDidMount() {
        const {name} = this.props;
        const location = new ClientLocationConstructor(name);

        this.location = location;

        location.onUpdate((clientPlugins: Array<IClientPluginsDescriptor>) => this.setState({clientPlugins}));

        registry.register(location);
    }

    render() {
        const {name, children} = this.props;
        const {clientPlugins} = this.state;

        if (!name) {
            console.log('ClientLocation Error: "name" has to be specified');
            return null;
        }

        if (!children || typeof children !== 'function') {
            console.log('ClientLocation Error: "children" is not defined or not a function');

            // Default render
            if (Array.isArray(clientPlugins)) {
                return clientPlugins.map((plugin: IClientPluginsDescriptor) => <ClientWebfragment key={plugin.id} descriptor={plugin} />);
            }

            return null;
        }

        if (!clientPlugins) {
            return null;
        }

        return children(clientPlugins) || null;
    }
}

export default ClientLocation;